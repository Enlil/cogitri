# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=redhat-performance tag=v${PV} ] \
    python [ blacklist='2 3.7' multibuild=false ] \
    systemd-service

export_exlib_phases src_install

SUMMARY="Tuning Profile Delivery Mechanism for Linux"
LICENCES="GPL-2"
SLOT="0"

MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-util/desktop-file-utils
    run:
        app-laptop/powertop
        dev-python/decorator[python_abis:*(-)?]
        dev-python/dbus-python[python_abis:*(-)?]
        dev-python/linux-procfs[python_abis:*(-)?]
        dev-python/pyudev[python_abis:*(-)?]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
        sys-apps/dbus
        sys-apps/ethtool
        sys-apps/hdparm
        sys-auth/polkit:1
    suggestion:
        sys-apps/dmidecode [[
                description = [ Use to discover the chassis type ]
        ]]
"

tuned_src_install() {
    emake \
        PKG_CONFIG=$(exhost --tool-prefix)pkg-config \
        PYTHON=python$(python_get_abi) \
        UNITDIR_FALLBACK="${SYSTEMDSYSTEMUNITDIR}" \
        TMPFILES_FALLBACK="${SYSTEMDTMPFILESDIR}" \
        TUNED_PROFILESDIR="/usr/$(exhost --target)/lib/tuned" \
        DESTDIR="${IMAGE}" \
        install

    # TODO: Upstream this...
    edo rm -r "${IMAGE}"/run
    edo mv "${IMAGE}"/usr/bin "${IMAGE}"/usr/$(exhost --target)/bin
    edo mv "${IMAGE}"/usr/sbin/* "${IMAGE}"/usr/$(exhost --target)/bin/
    edo rmdir "${IMAGE}"/usr/sbin
    edo mv "${IMAGE}"/usr/lib "${IMAGE}"/usr/$(exhost --target)/lib
    edo mv "${IMAGE}"/usr/libexec "${IMAGE}"/usr/$(exhost --target)/libexec
    edo mv "${IMAGE}"/usr/$(exhost --target)/lib/lib/* "${IMAGE}"/usr/$(exhost --target)/lib/
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib/lib

    keepdir /var/lib/tuned
    keepdir /var/log/tuned
    keepdir /etc/tuned/recommend.d
}

