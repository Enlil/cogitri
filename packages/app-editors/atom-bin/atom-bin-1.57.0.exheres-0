# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A hackable text editor for the 21st Century"
HOMEPAGE="https://atom.io"

DOWNLOADS="
    platform:amd64? ( https://atom-installer.github.com/v${PV}/atom-amd64.deb -> atom-amd64-${PV}.deb )
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="platform: amd64"

DEPENDENCIES="
    run:
        dev-libs/at-spi2-atk
        dev-libs/at-spi2-core
        dev-libs/atk
        dev-libs/expat
        dev-libs/glib:2
        dev-libs/libsecret:1
        dev-libs/nspr
        dev-libs/nss
        gnome-platform/GConf:2
        media-libs/fontconfig
        net-misc/curl
        net-print/cups
        sys-apps/dbus
        sys-apps/util-linux
        sys-libs/glibc
        sys-libs/libgcc:*
        sys-libs/zlib
        sys-sound/alsa-lib
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXScrnSaver
        x11-libs/libXtst
        x11-libs/libxcb
        x11-libs/libxkbfile
        x11-libs/pango
        x11-dri/libdrm
        x11-dri/mesa
"

WORK="${WORKBASE}"

src_unpack() {
    default

    edo tar xf data.tar.xz
}

src_test() {
    :
}

src_install() {
    insinto /usr/
    doins -r usr/share

    # set correct permissions for atom executable
    edo chmod 775 "${IMAGE}"/usr/share/atom/atom
    # set correct permissions for apm executable and its helpers
    edo chmod -R 775 "${IMAGE}"/usr/share/atom/resources/app/apm/bin/

    dodir /usr/$(exhost --target)/bin
    dosym /usr/share/atom/atom /usr/$(exhost --target)/bin/atom
    dosym /usr/share/atom/resources/app/apm/bin/apm /usr/$(exhost --target)/bin/apm

    edo find "${IMAGE}"/usr/share/ -type d -empty -delete
}

