# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import=setuptools multibuild=false blacklist="2" python_opts="[sqlite]" ]
require github [ user=BasioMeusPuga project=Lector tag=${PV} ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases src_compile src_install pkg_postrm pkg_postinst

SUMMARY="Qt based ebook reader"

LICENCES="GPL-3"
SLOT="0"

MYOPTIONS=""

DEPENDENCIES="
    build:
        x11-libs/qttools:5
    build+run:
        dev-python/beautifulsoup4[python_abis:*(-)?]
        dev-python/lxml[python_abis:*(-)?]
        dev-python/PyQt5[multimedia][python_abis:*(-)?]
        dev-python/urllib3[python_abis:*(-)?]
        dev-python/xmltodict[python_abis:*(-)?]
    suggestion:
        dev-python/pymupdf[python_abis:*(-)?] [[
            description = [ Support for readning PDFs ]
        ]]
"

lector_src_compile() {
    setup-py_src_compile

    edo pushd lector/resources/translations
    for translation in Lector_*.ts ; do
        edo lrelease-qt5 ${translation}
    done
    edo popd
}

lector_src_install() {
    setup-py_src_install

    insinto /usr/share/qt5/translations
    edo pushd lector/resources/translations
    doins Lector_*.qm
    edo popd
}

lector_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

lector_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

